###### Simple Q-Learning Program

In this program the ``O`` object try to find ``T`` treasure in the square box

 - - - 
|O| | |
 - - - 
| | | |
 - - - 
| | |T|
 - - -
